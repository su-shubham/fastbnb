import motor.motor_asyncio
from .models import BnB
from .config import settings

try:
    client = motor.motor_asyncio.AsyncIOMotorClient(settings.mongoDB_url)    
    collection = client.sample_airbnb
except Exception as e:
    print(e)

async def get_data():
    response=[]
    cursor = collection.listingsAndReviews.find({'cleaning_fee':{'$exists': True}}, limit=15)
    async for data in cursor:
        response.append(
            BnB(
              data['_id'],
              data['name'], 
              data['summary'], 
              data['address']['street'], 
              str(data['price']), 
              str(data['cleaning_fee']),
              str(data['accommodates']),
              data['images']['picture_url'],
              data['amenities'],
              data['property_type']
            )
        ) 
    return response

async def get_individual_info(id:str)->dict:
    data= await collection.listingsAndReviews.find_one({'_id': id})
    response=BnB(
              data['_id'],
              data['name'], 
              data['summary'], 
              data['address']['street'], 
              str(data['price']), 
              str(data['cleaning_fee']),
              str(data['accommodates']),
              data['images']['picture_url'],
              data['amenities'],
            data['property_type']
            )
    return response

async def confirm_book(id:str)->dict:
    confirm_data= await collection.bookings.insert_one({'property': id})
    return confirm_data
